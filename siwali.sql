-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2019 at 01:19 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siwali`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` varchar(20) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `catatan`
--

CREATE TABLE `catatan` (
  `id_catatan` int(11) NOT NULL,
  `isi` text,
  `nim_mahasiswa` varchar(8) DEFAULT NULL,
  `nip_dosen` varchar(18) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `nip_dosen` varchar(18) NOT NULL,
  `nama_dosen` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`nip_dosen`, `nama_dosen`) VALUES
('198606192015041001', 'Handoyo, S.Si., M.T.'),
('198705242015042001', 'Gestin Mey Ekawati, S.T., M.T.'),
('1987062720181100', 'Soni Satiawan, S.T., M.Sc.'),
('1987071220171070', 'Cahli Suhendi, S.Si., M.T.'),
('198710062015041003', 'Ruhul Firdaus, S.T., M.T.'),
('198801272018032000', 'Rizka, S.T., M.T.'),
('1989052220161000', 'Reza Rizki, S.T., M.T.'),
('1990090420182069', 'Harnanti Y Hutami, S.Si., M.T.'),
('1991041120172050', 'Maria R P Sudibyo, S.Si., M.Sc'),
('1991061020181090', 'Erlangga Ibrahim Fattah, S.Si.'),
('1991071720182080', 'Rhahmi Adni Pesma, S.Si., M.Si'),
('1991081420181100', 'Dr. Nono A S, S.Si., M.T.');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim_mahasiswa` varchar(8) NOT NULL,
  `nama_mahasiswa` varchar(30) NOT NULL,
  `angkatan` int(4) NOT NULL,
  `nip_dosen` varchar(18) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim_mahasiswa`, `nama_mahasiswa`, `angkatan`, `nip_dosen`) VALUES
('12112009', 'FX. Tunggul Sutrisno', 2012, '198710062015041003'),
('12113002', 'Dwi Anggun Bissabri', 2013, '198710062015041003'),
('12113004', 'Yanrizha Ihsan', 2013, '198705242015042001'),
('12113006', 'Muhammad Hadi Kurniawan', 2014, '198710062015041003'),
('12113007', 'Putri Ambarsari', 2013, '1989052220161000'),
('12114001', 'Fakhriza syahda', 2014, '198710062015041003'),
('12114003', 'M Hanif Syamri', 2014, '198705242015042001'),
('12114004', 'Muhammad Ahsani Taqwim', 2014, '198606192015041001'),
('12114005', 'Hendra hidayat akbar', 2014, '198710062015041003'),
('12115001', 'Febri A D Fernanda', 2015, '198705242015042001'),
('12115002', 'Indri Safitri', 2015, '198606192015041001'),
('12115003', 'Luqman thareq togak ratu', 2015, '198710062015041003'),
('12115004', 'Muhammad machfud', 2015, '1989052220161000'),
('12115005', 'Insanul Kamil', 2015, '198705242015042001'),
('12115006', 'Andho marendra', 2015, '198801272018032000'),
('12115007', 'Muhammad I Naufaldi', 2015, '198705242015042001'),
('12115009', 'Yoopy christian', 2015, '198710062015041003'),
('12115010', 'Rendra aditya hakim', 2015, '1989052220161000'),
('12115011', 'Beta A Piskora', 2015, '198705242015042001'),
('12115012', 'Gabrio hikma januarta', 2015, '198801272018032000'),
('12115013', 'Dayu Prabowo', 2015, '198705242015042001'),
('12115014', 'Jihan Aulia', 2015, '198606192015041001'),
('12115015', 'Rizky alpha putra', 2015, '198710062015041003'),
('12115016', 'Lidyana ardianti', 2015, '1989052220161000'),
('12115017', 'Putu Pradnya andika', 2015, '198705242015042001'),
('12115018', 'Putri wulandari', 2015, '198801272018032000'),
('12115019', 'Elysia Levina', 2015, '198705242015042001'),
('12115020', 'Kristina Manurung', 2015, '198606192015041001'),
('12115023', 'Asido Saputra Sigalingging', 2015, '198606192015041001'),
('12115024', 'Diana rizky yuliza', 2015, '198801272018032000'),
('12115025', 'Dita M Sihombing', 2015, '198705242015042001'),
('12115027', 'Roy limbong', 2015, '198710062015041003'),
('12115029', 'Maya Mardani', 2015, '198606192015041001'),
('12115031', 'Falah F K Kautsar', 2015, '198705242015042001'),
('12115032', 'Rifa Salma Salsabila', 2015, '198606192015041001'),
('12115033', 'Alvirda synthia', 2015, '198710062015041003'),
('12115034', 'Neneng risda ulfa', 2015, '1989052220161000'),
('12115035', 'Edlyn Yoadan Nathania', 2015, '198606192015041001'),
('12115036', 'Fernando gus anwar.m', 2015, '198801272018032000'),
('12115037', 'Nadya agnesia sinaga', 2015, '198705242015042001'),
('12115039', 'M luthfi risqulloh fadholi', 2015, '198710062015041003'),
('12115040', 'Dwi agustiyani', 2015, '1989052220161000'),
('12115041', 'Nurfaizi', 2015, '198801272018032000'),
('12115043', 'Elizabet Simanjuntak', 2015, '198705242015042001'),
('12115044', 'Ekky Riwandha Kurniawan', 2015, '198606192015041001'),
('12115046', 'Rizal bahrul ulum', 2015, '1989052220161000'),
('12115048', 'Muhammad latif biantoro', 2015, '198801272018032000'),
('12115050', 'Agustina Triwulandari', 2015, '198606192015041001'),
('12115051', 'Nisrina balqis maharani putri', 2015, '198710062015041003'),
('12115053', 'Bani habibi', 2015, '198801272018032000'),
('12115054', 'M. rafif pascaloa', 2015, '198801272018032000'),
('12115056', 'Ramot Fernando', 2015, '198606192015041001'),
('12115057', 'Ronaldo paulus ricky martin', 2015, '198710062015041003'),
('12115058', 'Iqbal niko abqori', 2015, '1989052220161000'),
('12115061', 'Melda Pratiwie', 2015, '198705242015042001'),
('12115062', 'Fitriyani', 2015, '198606192015041001'),
('12115063', 'Kristian n. hutagalung', 2015, '198710062015041003'),
('12115064', 'Muchamad andara', 2015, '1989052220161000'),
('12116001', 'Elan P. Sinaga', 2016, '198606192015041001'),
('12116002', 'Reza Syindhica', 2016, '198606192015041001'),
('12116003', 'Dwi Bulansari', 2016, '198606192015041001'),
('12116004', 'Aditya Dharma Dwipa', 2016, '198606192015041001'),
('12116005', 'Ahmad Rowatul Irham', 2016, '198606192015041001'),
('12116006', 'Randyana', 2016, '198606192015041001'),
('12116007', 'Felik Destian Putra Amijaya', 201, '198606192015041001'),
('12116008', 'Izhar Syafawi', 2016, '198606192015041001'),
('12116009', 'Febriani Dewi Catur Pamungkas', 2016, '198606192015041001'),
('12116010', 'Putri Sabila Damayant', 2016, '198606192015041001'),
('12116011', 'Mahardika Aji Kurniawan', 2016, '198606192015041001'),
('12116012', 'Ichsan Algifary', 2016, '198606192015041001'),
('12116013', 'M. Intasya Falie Rizqi', 2016, '198606192015041001'),
('12116014', 'Helen Zetri', 2016, '198606192015041001'),
('12116015', 'Benyamin Oozatulo Telaumbanua', 2016, '198710062015041003'),
('12116016', 'M Hendra Gunawan', 2016, '198710062015041003'),
('12116017', 'Tommy Afrizal Umar Cili', 2016, '198710062015041003'),
('12116018', 'Wahyu Eko Junian', 2016, '198710062015041003'),
('12116020', 'Muhammad Rafli Ananta', 2016, '198710062015041003'),
('12116021', 'Della Thesay Adellia', 2016, '198710062015041003'),
('12116022', 'Resti Ayu Nofitha', 2016, '198710062015041003'),
('12116023', 'Wardah Restianingsih', 2016, '198710062015041003'),
('12116025', 'Julita Anggelia Saputri', 2016, '198710062015041003'),
('12116026', 'Octi Octavia. Nz', 2016, '198710062015041003'),
('12116028', 'Nazaellya Tsabita Nurazisha', 2016, '198710062015041003'),
('12116029', 'Muhammad Rendi Pratama', 2016, '198705242015042001'),
('12116030', 'Handika Tampubolon', 2016, '198705242015042001'),
('12116031', 'Muhammad Irfan', 2016, '198705242015042001'),
('12116032', 'Windah Junianti Rambe', 2016, '198705242015042001'),
('12116033', 'Dita A Hutabalian', 2016, '198705242015042001'),
('12116034', 'Elfinder Manullang', 2016, '198705242015042001'),
('12116035', 'Fari Rizki', 2016, '198705242015042001'),
('12116036', 'Vira Seprina Putri', 2016, '198705242015042001'),
('12116037', 'Alwi Alaksa Parmanda', 2016, '198705242015042001'),
('12116038', 'Fitria Ramayanti', 2016, '198705242015042001'),
('12116039', 'Virgiwan Rivaldi', 2016, '198705242015042001'),
('12116040', 'Ambar Nabilla', 2016, '198705242015042001'),
('12116041', 'Yosevany Tarigan', 2016, '198705242015042001'),
('12116042', 'Mikha Parasian Gamaliel Tobing', 2016, '198801272018032000'),
('12116043', 'Reynaratri Wijayanti', 2016, '198801272018032000'),
('12116044', 'Muhammad Ali Al Habib', 2016, '198801272018032000'),
('12116045', 'Arvico Putraloka', 2016, '198801272018032000'),
('12116046', 'Cahya Nuari Ramadhan', 2016, '198801272018032000'),
('12116047', 'Alvi Rahma Lukitasari', 2016, '198801272018032000'),
('12116048', 'Wantika Tri Wahyudi', 2016, '198801272018032000'),
('12116049', 'Tria Nisrina Mufidah', 2016, '198801272018032000'),
('12116050', 'Tyas Sancana Ramadan', 2016, '198801272018032000'),
('12116052', 'Hana Yudi Perkasa', 2016, '198801272018032000'),
('12116053', 'Mahesha Ramadhan', 2016, '198801272018032000'),
('12116054', 'David Octavianus', 2016, '198801272018032000'),
('12116055', 'Meyka Fitria Ningrum', 2016, '198801272018032000'),
('12116056', 'Fiska Andani', 2016, '198801272018032000'),
('12116057', 'Lilis Misliana', 2016, '198801272018032000'),
('12116058', 'Arif Pratama Ramadhan', 2016, '198801272018032000'),
('12116060', 'Rizki Wulandari', 2016, '1989052220161000'),
('12116061', 'M Defransyah Yuliadi', 2016, '1989052220161000'),
('12116062', 'M. Fadel Hotman', 2016, '1989052220161000'),
('12116063', 'Gustika Indriani Yahya', 2016, '1989052220161000'),
('12116065', 'Andreas Pujian Sihombing', 2016, '1989052220161000'),
('12116067', 'Argiyangan Finanjaya', 2016, '1989052220161000'),
('12116069', 'Eka Setya Palupi', 2016, '1989052220161000'),
('12116070', 'Firdizky Wahyu Febriyanto', 2016, '1989052220161000'),
('12116071', 'Marchdofayana Pane', 2016, '1989052220161000'),
('12116072', 'Ericson Sijabat', 2016, '1989052220161000'),
('12116073', 'William Yan Healthy', 2016, '1989052220161000'),
('12116074', 'Muhamad Faiz', 2016, '1989052220161000'),
('12116075', 'Boi Sondang Meka', 2016, '1989052220161000'),
('12116076', 'Anis Nofita', 2016, '1991041120172050'),
('12116077', 'Jenny Gita Fransiska', 2016, '1991041120172050'),
('12116078', 'Benyamin Elilaski Nababan', 2016, '1991041120172050'),
('12116079', 'Novarisa Berliana Siahaan', 2016, '1987071220171070'),
('12116080', 'Hery Paskah Simanjuntak', 2016, '1987071220171070'),
('12116081', 'Nurul Fitriani', 2016, '1987071220171070'),
('12116082', 'Meli Mariana Siboro', 2016, '1987071220171070'),
('12116083', 'Mangidotua Manullang', 2016, '1987071220171070'),
('12116084', 'Teresia Okta Alvionita Br Sinu', 2016, '1987071220171070'),
('12116085', 'Sadrak Siregar', 2016, '1989052220161000'),
('12116086', 'Trisnawati Juwita Tampubolon', 2016, '1991041120172050'),
('12116087', 'Diki Candra Pilly', 2016, '1991041120172050'),
('12116089', 'Oktaria Futri Ilhami', 2016, '1991041120172050'),
('12116090', 'Gita Rusmala', 2016, '1991041120172050'),
('12116091', 'Dian Puspita', 2016, '1991041120172050'),
('12116092', 'Tiar Evimaria Tampubolon', 2016, '1991041120172050'),
('12116093', 'Lara Ariska', 2016, '1991041120172050'),
('12116094', 'Muhammad Artfanton Mahartanto', 2016, '1991041120172050'),
('12116095', 'Ferdana Velon Munthe', 2016, '1991041120172050'),
('12116096', 'Safna Ramadhani', 2016, '1991041120172050'),
('12116097', 'Alfalah Adam Saputra', 2016, '1991041120172050'),
('12116098', 'Masmur Palentino', 2016, '1991041120172050'),
('12116099', 'Defri Kurniawan', 2016, '1991041120172050'),
('12116100', 'Ervan Prasetyo', 2016, '1991041120172050'),
('12116101', 'Alvin Firmansyah', 2016, '1991041120172050'),
('12116102', 'Ermounda Desilva', 2016, '1991041120172050'),
('12116103', 'Maria Raffelia Pardede', 2016, '1991041120172050'),
('12116104', 'Aditya Fathur Rahman', 2016, '1991041120172050'),
('12116105', 'Reinaldi Samuel Tua Manik', 2016, '1991041120172050'),
('12116106', 'Fitri Cahya Wulan', 2016, '1991041120172050'),
('12116107', 'Chicha Ayu Safitri', 2016, '1991041120172050'),
('12116108', 'Anna S.sitohang', 2016, '1991041120172050'),
('12116109', 'Nisrina Zalfa Syariefah', 2016, '1991041120172050'),
('12116110', 'Parsaoran Marito Hutagaol', 2016, '1991041120172050'),
('12116111', 'Lasroha M Panjaitan', 2016, '198705242015042001'),
('12116113', 'Aviv Alansyah', 2016, '198705242015042001'),
('12116114', 'Novia Risnawati', 2016, '198705242015042001'),
('12116115', 'Shavira Widya Pangestika', 2016, '198606192015041001'),
('12116116', 'Yuriza Alvionita', 2016, '198606192015041001'),
('12116118', 'M. Irvan', 201, '198606192015041001'),
('12116119', 'Muhammad Fadillah Harahap', 2016, '198801272018032000'),
('12116120', 'Rini Exprianti Sinaga', 2016, '198801272018032000'),
('12116121', 'Jhonrio A H C Pane', 2016, '198801272018032000'),
('12116122', 'Muhammad Imam Arif', 2016, '198801272018032000'),
('12116123', 'Adelia Gita Parera', 2016, '1989052220161000'),
('12116124', 'Resti Laila Syari', 2016, '1989052220161000'),
('12116125', 'M Rizky Yudhaprasetyo', 2016, '1989052220161000'),
('12116126', 'Fajri Gilang Maynaldi', 2016, '1989052220161000'),
('12116128', 'Lezy Nur Utari', 2016, '198710062015041003'),
('12116129', 'Farras Alwan Putra Yuga', 2016, '198710062015041003'),
('12116130', 'Hendra Rizki Fatoni', 2016, '198710062015041003'),
('12116131', 'Devi Riza Kurniasih', 2016, '1991041120172050'),
('12116132', 'Fellia Adika Anggita', 2016, '1991041120172050'),
('12116133', 'Kris Hamonangan Parulian David', 2016, '1991041120172050'),
('12116134', 'Siska Parulian Sitompul', 2016, '1991041120172050'),
('12116135', 'Avio Qiendy Mandela', 2016, '1987071220171070'),
('12116136', 'Lethfi Wahyu Akhbar', 2016, '1987071220171070'),
('12116137', 'Jamalul Ikhsan', 2016, '1987071220171070'),
('12116138', 'Doni Orlando', 2016, '1987071220171070'),
('12116139', 'Taufiq Brilliant Askari', 2016, '1987071220171070'),
('12116140', 'Ivan Zamorano Saputra Sitepu', 2016, '1987071220171070'),
('12116142', 'Dimas Aulya Ramadani', 2016, '1987071220171070'),
('12116143', 'Gabriel Dian Abednego', 2016, '1987071220171070'),
('12116144', 'Mohammad Hafizh Nur Irsyadilla', 2016, '1987071220171070'),
('12116145', 'Eldwin Manalu', 2016, '1987071220171070'),
('12116146', 'Shinta Carolina Sitinjak', 2016, '1987071220171070'),
('12116147', 'Panji Prasetyo', 2016, '1987071220171070'),
('12116148', 'Rio Natan Suryadi Panjaitan', 2016, '1987071220171070'),
('12116149', 'Elisabeth Sinaga', 2016, '1987071220171070'),
('12116151', 'Amy Vagara Simanullang', 2016, '1987071220171070'),
('12116152', 'Refmon Zikri Herdiwansyah', 2016, '1987071220171070'),
('12116154', 'Alfian Fauzan Akbari Ali', 2016, '1987071220171070'),
('12116155', 'Nugroho Prasetyo', 2016, '1987071220171070'),
('12116156', 'Oktavian Teguh Bagaskara', 2016, '1987071220171070'),
('12116157', 'Barca Yudha Setiawan', 2016, '1987071220171070'),
('12116158', 'Resy Dwi Laras', 2016, '1987071220171070'),
('12116159', 'Amal Nur Ikhsan', 2016, '1987071220171070'),
('12116160', 'Prana Al Mahkya', 2016, '1987071220171070'),
('12116165', 'Rani Meiliana', 2016, '1987071220171070'),
('12117001', 'Rizqi Chudori', 2017, '1990090420182069'),
('12117002', 'Yolanda Elok Sasmitasari', 2017, '1991061020181090'),
('12117003', 'Oktaviani Rohayu', 2017, '1991081420181100'),
('12117005', 'Muhammad Teguh Rianto', 2017, '1987062720181100'),
('12117006', 'Devi Yulia Anggraeny', 2017, '1991071720182080'),
('12117007', 'Ganang Panggayuh', 2017, '1991071720182080'),
('12117008', 'Rizky Setiawan', 2017, '1990090420182069'),
('12117009', 'Lestari Sukma Apriliana', 2017, '1991061020181090'),
('12117010', 'Reni Wijaya', 2017, '1987062720181100'),
('12117011', 'Mustika Dharma Ningtyas', 2017, '1991061020181090'),
('12117012', 'Diah Ayu Widyani', 2017, '1991061020181090'),
('12117013', 'Dwinda Aldatri', 2017, '1991061020181090'),
('12117014', 'Seni Gustiani', 2017, '1987062720181100'),
('12117015', 'Fatkhur Rohman', 2017, '1990090420182069'),
('12117016', 'Uut Dwi Ulfa', 2017, '1991071720182080'),
('12117017', 'Fortis L.G Sianturi', 2017, '1991071720182080'),
('12117018', 'Rosakanina Gusniarti', 2017, '1991061020181090'),
('12117019', 'Deanatha A Ramadhini', 2017, '1990090420182069'),
('12117020', 'Indah Nurhayati', 2017, '1990090420182069'),
('12117022', 'Evi Oktariswati', 2017, '1991081420181100'),
('12117023', 'Yohana Stefani Boka S.', 2017, '1987062720181100'),
('12117024', 'Puji Lestari', 2017, '1987062720181100'),
('12117025', 'Mustika', 2017, '1987062720181100'),
('12117026', 'Elda Deka Mayestri', 2017, '1991061020181090'),
('12117027', 'Annisa Nurhidayati', 2017, '1990090420182069'),
('12117028', 'Ariefa Radhitya', 2017, '1990090420182069'),
('12117029', 'Alan Pangestu', 2017, '1991081420181100'),
('12117030', 'Winda Lourenza Hutagalung', 2017, '1991061020181090'),
('12117031', 'Hayatun Nufus Hukama', 2017, '1991061020181090'),
('12117032', 'Sali Wahyudi', 2017, '1991061020181090'),
('12117033', 'Venny Rachmasari', 2017, '1991061020181090'),
('12117034', 'Zohria Oktasari', 2017, '1991061020181090'),
('12117035', 'Novia Purnama Suci', 2017, '1991061020181090'),
('12117036', 'Nurlailatun Hikmah Arona', 2017, '1991071720182080'),
('12117037', 'Amanda Sapphira Yasmine', 2017, '1987062720181100'),
('12117038', 'Malik Abdul Aziz', 2017, '1991061020181090'),
('12117039', 'Andre Lausesa', 2017, '1991081420181100'),
('12117040', 'Tri Andini', 2017, '1990090420182069'),
('12117041', 'Santo Tri Prabowo', 2017, '1991081420181100'),
('12117042', 'Anggreini Siburian', 2017, '1991071720182080'),
('12117043', 'Anjali Syalsabilah Putri', 2017, '1991081420181100'),
('12117044', 'Rizca Vanden Bokshow', 2017, '1990090420182069'),
('12117045', 'Chairul Anwar', 2017, '1991081420181100'),
('12117046', 'Robian Asmaul Fajri', 2017, '1991061020181090'),
('12117047', 'Evita Putri Fadhillah', 2017, '1991061020181090'),
('12117048', 'Widya Kirana Gunawan', 2017, '1990090420182069'),
('12117049', 'Dea Galuh Cahyaningrum', 2017, '1991061020181090'),
('12117050', 'Wahyu Hapiz Saputra', 2017, '1991061020181090'),
('12117051', 'Noer Muhammad Farhan', 2017, '1990090420182069'),
('12117052', 'Ferdynan Husien', 2017, '1990090420182069'),
('12117053', 'Muh Labib Zamakhsyari', 2017, '1987062720181100'),
('12117054', 'Lady Yaumilia', 2017, '1987062720181100'),
('12117055', 'Muhammad Alief Darmawan', 2017, '1987062720181100'),
('12117056', 'Iga Ayu Octaviana', 2017, '1991081420181100'),
('12117057', 'Eliza Veronica Zanetta', 2017, '1987062720181100'),
('12117058', 'Liris Ajeng Anggreini', 2017, '1991061020181090'),
('12117059', 'Yohanes Novendra Dwiyan Silala', 2017, '1991061020181090'),
('12117060', 'Rury Delvatiwi Martianda', 2017, '1991061020181090'),
('12117061', 'Muhammad Refi Gumara', 2017, '1991071720182080'),
('12117062', 'Dian Zulpa', 2017, '1990090420182069'),
('12117063', 'Ari Pamuka Dewa', 2017, '1991081420181100'),
('12117064', 'Muhammad Irfan', 2017, '1991061020181090'),
('12117065', 'Muhammad Iqbal Adri', 2017, '1991081420181100'),
('12117067', 'Naufal Hilmi Mubarok', 2017, '1991061020181090'),
('12117068', 'Ari Dita Anggraini', 2017, '1991081420181100'),
('12117069', 'Efraim Fernandes Saragih', 2017, '1991071720182080'),
('12117071', 'Fricha Sundang Maulana', 2017, '1991061020181090'),
('12117072', 'Oktavia Nelson', 2017, '1991081420181100'),
('12117073', 'Aserbrema Barus', 2017, '1987062720181100'),
('12117074', 'Alfian', 2017, '1990090420182069'),
('12117075', 'Kadek Julia Astiti', 2017, '1990090420182069'),
('12117076', 'Muda Setiwawan M S', 2017, '1991071720182080'),
('12117077', 'Fahri Rafif Waskito', 2017, '1991071720182080'),
('12117078', 'Ardi Muhammad', 2017, '1991081420181100'),
('12117079', 'Yola Wulanda Masri', 2017, '1991081420181100'),
('12117080', 'Muhammad Alrendy Sahay', 2017, '1991071720182080'),
('12117081', 'Yoas Krisanto Ambarita', 2017, '1991071720182080'),
('12117082', 'Yeni Ester Octavia', 2017, '1991061020181090'),
('12117083', 'Alhumamy Ihza Anindhita', 2017, '1990090420182069'),
('12117084', 'Anggraeni Mulyandari', 2017, '1991081420181100'),
('12117085', 'Ayyas Fathurahman', 2017, '1991071720182080'),
('12117086', 'Alfa Riezky Priyaditya', 2017, '1990090420182069'),
('12117087', 'Edo Kurnia Amirio', 2017, '1990090420182069'),
('12117088', 'Sri Anggraini', 2017, '1991081420181100'),
('12117089', 'Gopindo Wahyu Samosir', 2017, '1987062720181100'),
('12117090', 'Wisnu Prayudha', 2017, '1987062720181100'),
('12117091', 'Annisa Farah', 2017, '1991071720182080'),
('12117092', 'Fahmi Ilhamsyah', 2017, '1990090420182069'),
('12117093', 'Muthiah Sari', 2017, '1991061020181090'),
('12117094', 'Agastya Pramadya', 2017, '1991071720182080'),
('12117095', 'Nahdah Novia', 2017, '1987062720181100'),
('12117096', 'Novta Artila Sipayung', 2017, '1991071720182080'),
('12117097', 'Juanda Butarbutar', 2017, '1987062720181100'),
('12117098', 'Julianus Edgar', 2017, '1987062720181100'),
('12117099', 'Dwiki Viergoesta Rahmad', 2017, '1991061020181090'),
('12117100', 'Andreas Sohmono Berutu', 2017, '1987062720181100'),
('12117101', 'Muhammad Rizqi Efrian', 2017, '1991071720182080'),
('12117102', 'Aqil Muqaffi R A Mahmudy', 2017, '1987062720181100'),
('12117103', 'Patrick Gideon Sirait', 2017, '1987062720181100'),
('12117104', 'Wirah Wizendro', 2017, '1991081420181100'),
('12117105', 'Nisa Hamdani Pratiwi', 2017, '1991081420181100'),
('12117106', 'Francisco Martin', 2017, '1991081420181100'),
('12117107', 'Iqbal Suwarno', 2017, '1990090420182069'),
('12117108', 'Agungsewu Ajie Pratama', 2017, '1990090420182069'),
('12117109', 'Teguh Tirta Fadhilah', 2017, '1990090420182069'),
('12117110', 'Fathurahman A Siregar', 2017, '1987062720181100'),
('12117111', 'Pangeran G Lumbantoruan', 2017, '1987062720181100'),
('12117112', 'Anggiat Paul Napitupulu', 2017, '1991071720182080'),
('12117113', 'M.Wahid Khoirun Nahar', 2017, '1991071720182080'),
('12117114', 'Azura Nur Azmi', 2017, '1987062720181100'),
('12117115', 'Sonia Romaito Pasaribu', 2017, '1991071720182080'),
('12117116', 'Rivky Amierullah Rambe', 2017, '1991071720182080'),
('12117117', 'Ridwan Ramadhan', 2017, '1991061020181090'),
('12117118', 'Rafiq Perdana Latif', 2017, '1990090420182069'),
('12117119', 'Remon', 2017, '1991081420181100'),
('12117120', 'Thariq Ahsanul', 2017, '1991081420181100'),
('12117121', 'Siti Rina Yuliana Hasibuan', 2017, '1991071720182080'),
('12117122', 'Fiko Bradi Wibowo', 2017, '1990090420182069'),
('12117123', 'Desta Nandiyasari', 2017, '1990090420182069'),
('12117124', 'Yemima Nainggolan', 2017, '1991071720182080'),
('12117125', 'Mutiara Alfatihah', 2017, '1990090420182069'),
('12117126', 'Atika Mega Sri Wahyuni', 2017, '1991081420181100'),
('12117127', 'Alex F Munthe', 2017, '1987062720181100'),
('12117128', 'Michael Febrian Mardongan', 2017, '1991071720182080'),
('12117129', 'Restu Wika Bina', 2017, '1990090420182069'),
('12117130', 'Agnez Ridha Zatafatila', 2017, '1991061020181090'),
('12117131', 'Didian Noveni Waruwu', 2017, '1987062720181100'),
('12117132', 'Rahmita Fitri Ardela', 2017, '1990090420182069'),
('12117133', 'Liendo Diva S Aruan', 2017, '1987062720181100'),
('12117134', 'Kevin Heshell', 2017, '1991071720182080'),
('12117135', 'Rosa Elida Purba', 2017, '1987062720181100'),
('12117136', 'Lewinda Gultom', 2017, '1987062720181100'),
('12117137', 'Sandro Manurung', 2017, '1991071720182080'),
('12117139', 'Vera Sarah Simatupang', 2017, '1987062720181100'),
('12117140', 'vinsensius Irwanto R', 2017, '1987062720181100'),
('12117141', 'Diki Setyawan', 2017, '1991081420181100'),
('12117142', 'Bayu Aria Mahardika', 2017, '1991081420181100'),
('12117143', 'Muhammad Ichsan', 2017, '1991081420181100'),
('12117144', 'Dafa Febriansyah', 2017, '1991071720182080'),
('12117145', 'Putri Iska Aldina', 2017, '1991081420181100'),
('12117146', 'Danny Akbar', 2017, '1991081420181100'),
('12117147', 'Citra Puspita Sari', 2017, '1991081420181100'),
('12117150', 'Salsabila Tsania Putri', 2017, '1987071220171070'),
('12117151', 'Fira Pratiwi Darsono', 2017, '198710062015041003');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(10) NOT NULL,
  `subjek` varchar(1000) DEFAULT NULL,
  `jadwal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `level`) VALUES
(1, '14116102', '14116102', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catatan`
--
ALTER TABLE `catatan`
  ADD PRIMARY KEY (`id_catatan`),
  ADD KEY `nip_dosen` (`nip_dosen`),
  ADD KEY `nim_mahasiswa` (`nim_mahasiswa`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nip_dosen`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim_mahasiswa`),
  ADD KEY `nip_dosen` (`nip_dosen`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `catatan`
--
ALTER TABLE `catatan`
  ADD CONSTRAINT `catatan_ibfk_1` FOREIGN KEY (`nip_dosen`) REFERENCES `dosen` (`nip_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catatan_ibfk_2` FOREIGN KEY (`nim_mahasiswa`) REFERENCES `mahasiswa` (`nim_mahasiswa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`nip_dosen`) REFERENCES `dosen` (`nip_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
